import { ApexOptions } from "apexcharts";

export const chartOptions: ApexOptions = {
  plotOptions: {
    radialBar: {
      startAngle: 0,
      endAngle: 90,
      track: {
        // strokeWidth: "100%",
        // margin: 45,
        background: "#454545",
      },
      hollow: {
        margin: 5,
        size: "60%",
        background: "#212121",
      },
      dataLabels: {
        name: {
          show: false,
        },
        value: {
          show: false,
        },
      },
    },
  },
  fill: {
    type: "gradient",
    gradient: {
      shade: "dark",
      type: "horizontal",
      shadeIntensity: 0.5,
      gradientToColors: ["#FFCAC9"],
      inverseColors: true,
      opacityFrom: 1,
      opacityTo: 1,
      stops: [0, 50],
    },
  },
  stroke: {
    lineCap: "butt",
  },
  colors: ["#737DFE", "#454545"],
};
