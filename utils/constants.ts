import { FlowPeakCardTypes } from "@/components/core/FlowPeakCard";
import { ClubIcon, GameIcon, LessionIcon, ToolsIcon } from "./icons";

export const FLOW_PEAKS = [
  {
    name: "Night",
    startingRange: 75,
    endingRange: 100,
    startTime: 21,
    endTime: 3,
    userActivity: [],
  },
  {
    name: "Morning",
    startingRange: 0,
    endingRange: 25,
    startTime: 3,
    endTime: 9,
    userActivity: [
      {
        startActiveHr: 3,
        endActiveHr: 4.5,
      },
      {
        startActiveHr: 6,
        endActiveHr: 8,
      },
    ],
  },
  {
    name: "Afternoon",
    startingRange: 25,
    endingRange: 50,
    startTime: 9,
    endTime: 15,
    userActivity: [
      {
        startActiveHr: 10,
        endActiveHr: 12,
      },
      {
        startActiveHr: 13.5,
        endActiveHr: 15,
      },
    ],
  },
  {
    name: "Evening",
    startingRange: 50,
    endingRange: 75,
    startTime: 15,
    endTime: 21,
    userActivity: [
      {
        startActiveHr: 15.5,
        endActiveHr: 21,
      },
    ],
  },
];

export const TIMES_OF_DAY = [
  {
    name: "Night",
    position: "top-0",
  },
  {
    name: "Morning",
    position: "right-0",
  },
  {
    name: "Afternoon",
    position: "bottom-0",
  },
  {
    name: "Evening",
    position: "left-0",
  },
];

export const FLOW_PEAK: FlowPeakCardTypes[] = [
  {
    title: "My Flow Peaks",
    duration: "02PM - 08PM",
  },
  {
    title: "Global Flow Peaks",
    duration: "04PM - 11PM",
  },
];

export const TOOLKIT_DATA = [
  {
    name: "Games",
    forYou: "Explore",
    globally: "Memory Game",
    icon: GameIcon,
  },
  {
    name: "Tools",
    forYou: "Affirmation",
    globally: "Thought Guides",
    icon: ToolsIcon,
  },
  {
    name: "Lessons",
    forYou: "Lorem Ipsum",
    globally: "Lorem Ipsum",
    icon: LessionIcon,
  },
  {
    name: "Clubs",
    forYou: "Lorem Ipsum",
    globally: "Lorem Ipsum",
    icon: ClubIcon,
  },
];
