import { IRange } from "@/components/core/CircularProgress";
import { FLOW_PEAKS } from "./constants";
import { ConvertActiveHourToProgressRange } from "./ConvertActiveHourToProgressRange";

export const Range = () => {
  const range: IRange[] = [];

  FLOW_PEAKS.forEach((flowPeak) => {
    const { startingRange, endingRange, startTime, endTime, userActivity } =
      flowPeak;

    userActivity.forEach(({ startActiveHr, endActiveHr }) => {
      const { startingPoint, endingPoint } = ConvertActiveHourToProgressRange({
        startingRange,
        endingRange,
        startTime,
        endTime,
        startActiveHr,
        endActiveHr,
      });

      range.push({ startingPoint, endingPoint });
    });
  });

  return range;
};
