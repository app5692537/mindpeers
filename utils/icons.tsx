import * as React from "react";

export const InfoIcon = () => (
  <svg width={16} height={16} viewBox="0 0 14 14" fill="none">
    <path
      d="M6.342.018a7.026 7.026 0 0 0-4.283 2.024 7.002 7.002 0 0 0 0 9.919c1.112 1.11 2.469 1.775 4.09 2.005.327.047 1.404.044 1.74-.002 1.59-.22 2.984-.905 4.08-2.006a6.95 6.95 0 0 0 2.05-4.71 6.968 6.968 0 0 0-2.053-5.206A6.993 6.993 0 0 0 7.986.053C7.65.003 6.678-.018 6.341.018ZM7.35 3.74c.45.211.537.745.175 1.055-.156.134-.337.189-.578.173-.4-.028-.682-.3-.682-.658-.003-.477.605-.795 1.085-.57Zm.066 2.17c.082.03.175.09.224.148l.085.096v1.852c0 2.148.025 1.965-.296 2.123-.164.083-.205.091-.416.091-.274-.003-.458-.071-.606-.222l-.093-.096-.008-1.858L6.3 6.191l.063-.096c.165-.247.653-.332 1.053-.184Z"
      fill="url(#a)"
    />
    <defs>
      <linearGradient
        id="a"
        x1={16.861}
        y1={22.707}
        x2={-1.973}
        y2={-5.25}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#2F80ED" />
        <stop offset={1} stopColor="#B2FFDA" />
      </linearGradient>
    </defs>
  </svg>
);

export const ClockIcons = () => (
  <svg width={16} height={16} viewBox="0 0 16 16" fill="none">
    <g clipPath="url(#a)">
      <path
        d="M7.235.036a8.08 8.08 0 0 0-4.07 1.594c-.393.297-1.243 1.144-1.534 1.534C.86 4.196.363 5.321.144 6.514c-.419 2.263.1 4.47 1.487 6.325.291.391 1.141 1.238 1.535 1.535 1.137.85 2.4 1.365 3.85 1.572.369.053 1.597.05 1.984 0 1.81-.25 3.428-1.053 4.66-2.31.393-.4.487-.506.737-.837.813-1.072 1.344-2.385 1.547-3.813.053-.368.053-1.6 0-1.968-.181-1.263-.625-2.46-1.281-3.438A8.022 8.022 0 0 0 9.11.077C8.725.02 7.616-.004 7.235.037ZM8.29 2.952a.556.556 0 0 1 .197.194l.075.128V7.72l1.375 1.1c1.031.825 1.387 1.125 1.428 1.21a.797.797 0 0 1 .053.274c0 .3-.244.54-.553.54a.712.712 0 0 1-.25-.046c-.14-.069-3.022-2.388-3.097-2.494-.066-.087-.066-.125-.075-2.56l-.006-2.468.068-.122a.585.585 0 0 1 .572-.284.698.698 0 0 1 .213.08Z"
        fill="url(#b)"
      />
    </g>
    <defs>
      <linearGradient
        id="b"
        x1={-2.8}
        y1={-2.798}
        x2={17.6}
        y2={19.602}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#FA7CBB" />
        <stop offset={1} stopColor="#F14658" />
      </linearGradient>
      <clipPath id="a">
        <path fill="#fff" d="M0 0h16v16H0z" />
      </clipPath>
    </defs>
  </svg>
);

export const ChevronIcon = () => (
  <svg width={12} height={7} viewBox="0 0 12 7" fill="none">
    <path
      d="M.64.02a.875.875 0 0 0-.573.479.628.628 0 0 0-.063.34c0 .376-.226.122 2.798 3.151C4.627 5.817 5.526 6.702 5.6 6.742c.17.091.37.124.549.086.08-.016.195-.056.253-.086.073-.04.972-.925 2.797-2.752C12.222.961 11.996 1.215 11.996.84a.66.66 0 0 0-.066-.346.853.853 0 0 0-1.152-.401c-.061.03-.911.864-2.435 2.385L6 4.818l-2.343-2.34C2.127.95 1.283.123 1.222.093A.916.916 0 0 0 .64.02Z"
      fill="url(#a)"
    />
    <defs>
      <linearGradient
        id="a"
        x1={6.183}
        y1={8.212}
        x2={6.183}
        y2={-2.052}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#B2FFDA" />
        <stop offset={1} stopColor="#2F80ED" />
      </linearGradient>
    </defs>
  </svg>
);

export const LocationIcon = () => (
  <svg width={12} height={16} viewBox="0 0 9 12" fill="none">
    <path
      d="M4.164 0A4.17 4.17 0 0 0 0 4.165c0 2.21 3.778 7.374 3.94 7.593l.15.204a.093.093 0 0 0 .15 0l.15-.204c.16-.22 3.939-5.383 3.939-7.593A4.17 4.17 0 0 0 4.164 0Zm0 2.673c.823 0 1.492.669 1.492 1.492 0 .822-.67 1.491-1.492 1.491s-1.492-.67-1.492-1.491c0-.823.67-1.492 1.492-1.492Z"
      fill="url(#aL)"
    />
    <defs>
      <linearGradient
        id="aL"
        x1={4.164}
        y1={0}
        x2={4.164}
        y2={12}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#FA7CBB" />
        <stop offset={1} stopColor="#F14658" />
      </linearGradient>
    </defs>
  </svg>
);

export const GloballyIcon = () => (
  <svg width={16} height={16} viewBox="0 0 12 12" fill="none">
    <path
      d="M9.164.352v.351h1.629l-.996.999c-.549.548-1.31 1.303-1.695 1.678l-.698.684L6 2.66 4.596 1.256l-1.929 1.92L.422 5.407l-.317.312.254.25.25.254 1.566-1.559c.86-.858 1.755-1.748 1.992-1.98l.427-.422 1.404 1.404 1.406 1.406 1.94-1.931 1.941-1.93.007.8.005.801H12V0H9.164v.352Z"
      fill="url(#aG)"
    />
    <path
      d="M9.164 7.758v3.539h-.703V7.03H6.352v4.266h-.704V5.625H3.54v5.672h-.703V7.734H.703v3.562H0V12h12v-.703h-.703V4.219H9.164v3.539Z"
      fill="url(#bG)"
    />
    <defs>
      <linearGradient
        id="aG"
        x1={6.053}
        y1={6.223}
        x2={6.053}
        y2={0}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#FA7CBB" />
        <stop offset={1} stopColor="#F14658" />
      </linearGradient>
      <linearGradient
        id="bG"
        x1={6}
        y1={12}
        x2={6}
        y2={4.219}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#FA7CBB" />
        <stop offset={1} stopColor="#F14658" />
      </linearGradient>
    </defs>
  </svg>
);

export const GameIcon = () => (
  <svg width={23} height={16} viewBox="0 0 19 12" fill="none">
    <path
      d="M4.54.019c-1.083.11-2.03.744-2.77 1.856C.856 3.246.268 4.95.045 6.87c-.067.588-.057 2.154.018 2.764.127 1.02.304 1.551.637 1.92.408.443 1.113.574 1.669.312.489-.23 1.382-1.003 2.2-1.9.864-.945.89-.973 1.127-1.08l.22-.102h6.27l.23.11c.188.088.29.173.55.456 1.424 1.563 2.164 2.232 2.805 2.526.223.103.29.117.55.117.963 0 1.46-.602 1.672-2.023.18-1.233.163-2.625-.053-3.914-.373-2.24-1.467-4.433-2.661-5.343-.652-.496-1.265-.705-2.066-.705-.892 0-1.771.29-2.689.89a2.88 2.88 0 0 1-.51.258c-.22.074-.294.081-.963.081-.67 0-.744-.007-.964-.081a2.616 2.616 0 0 1-.496-.252C6.797.387 5.975.075 5.295.03a9.116 9.116 0 0 1-.39-.028 3.345 3.345 0 0 0-.364.018Zm8.532 2.14a.996.996 0 0 1 .379 1.282c-.16.308-.546.542-.897.542-.347 0-.715-.252-.875-.599-.074-.16-.085-.226-.07-.443.017-.272.074-.41.248-.605.283-.326.829-.408 1.215-.178Zm-7.6.612.195.064.01.61.008.609h1.169l.096.184c.13.252.138.592.014.886l-.082.205H5.614v1.208l-.166.078c-.252.117-.713.11-.922-.007l-.152-.088V5.329h-1.19l-.057-.117a1.287 1.287 0 0 1-.014-1.03l.053-.128H4.41v-.62c0-.34.003-.62.01-.62.004 0 .071-.025.149-.053.195-.071.662-.064.903.01Zm10.058.847a.977.977 0 0 1 .16 1.545c-.163.156-.45.273-.67.273-.343 0-.723-.241-.886-.567-.092-.177-.11-.521-.039-.73a.986.986 0 0 1 1.435-.52Zm-2.526 1.208c.326.167.51.454.539.833.014.216.007.258-.1.47-.19.39-.524.575-.974.54-.513-.043-.871-.45-.864-.982a.91.91 0 0 1 .531-.85c.206-.106.252-.114.475-.1.16.008.301.043.393.09Z"
      fill="url(#aGame)"
    />
    <defs>
      <linearGradient
        id="aGame"
        x1={8.627}
        y1={-1.941}
        x2={8.627}
        y2={14.018}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#FAD6A6" />
        <stop offset={1} stopColor="#F06966" />
      </linearGradient>
    </defs>
  </svg>
);

export const ToolsIcon = () => (
  <svg width={16} height={16} viewBox="0 0 12 12" fill="none">
    <path
      d="M1.46.022C.73.149.247.58.05 1.289.007 1.453 0 1.594 0 2.56s.006 1.107.05 1.27c.193.69.639 1.104 1.339 1.245.251.054.361.057 1.264.046 1.101-.012 1.169-.02 1.567-.212.406-.198.703-.584.838-1.092.046-.175.051-.32.051-1.27 0-1.037-.003-1.082-.065-1.3A1.587 1.587 0 0 0 4.615.5a1.601 1.601 0 0 0-.734-.426c-.2-.06-.265-.065-1.242-.071-.567-.003-1.098.006-1.18.02Z"
      fill="url(#aTool)"
    />
    <path
      d="M8.412.01c-.81.122-1.327.602-1.485 1.384-.054.25-.056.36-.045 1.264.011 1.101.02 1.169.212 1.567.138.288.395.545.683.683.398.192.465.2 1.566.212.903.01 1.014.008 1.265-.046.7-.14 1.146-.556 1.338-1.244.045-.164.05-.305.05-1.27 0-.966-.005-1.107-.05-1.27-.18-.647-.579-1.045-1.22-1.232-.144-.042-.319-.05-1.2-.053A21.734 21.734 0 0 0 8.413.01Z"
      fill="url(#bTool)"
    />
    <path
      d="M1.397 6.92C.7 7.06.26 7.46.065 8.132.003 8.35 0 8.395 0 9.445c0 .966.006 1.107.05 1.27.181.647.588 1.05 1.234 1.234.164.045.305.051 1.27.051.966 0 1.107-.006 1.27-.05.69-.193 1.104-.639 1.246-1.339.053-.254.056-.361.045-1.278-.009-.94-.014-1.017-.074-1.214-.15-.497-.42-.83-.832-1.03-.407-.195-.54-.212-1.66-.21-.678.003-1.025.015-1.152.04Z"
      fill="url(#cTool)"
    />
    <path
      d="M8.27 6.92c-.57.126-.97.422-1.185.871-.183.387-.192.44-.203 1.513-.014 1.053 0 1.248.124 1.595.15.42.418.717.81.906.37.178.505.195 1.626.195.861 0 1.033-.008 1.214-.05.686-.162 1.104-.565 1.287-1.251.048-.175.054-.308.054-1.268 0-1.036-.003-1.08-.065-1.298-.178-.616-.579-1.01-1.206-1.191-.144-.043-.324-.048-1.227-.054-.805-.006-1.099.003-1.228.031Z"
      fill="url(#dTool)"
    />
    <defs>
      <linearGradient
        id="aTool"
        x1={2.555}
        y1={5.125}
        x2={2.555}
        y2={0.001}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#F06966" />
        <stop offset={1} stopColor="#FAD6A6" />
      </linearGradient>
      <linearGradient
        id="bTool"
        x1={9.437}
        y1={5.125}
        x2={9.437}
        y2={0.002}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#F06966" />
        <stop offset={1} stopColor="#FAD6A6" />
      </linearGradient>
      <linearGradient
        id="cTool"
        x1={2.56}
        y1={12}
        x2={2.56}
        y2={6.879}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#F06966" />
        <stop offset={1} stopColor="#FAD6A6" />
      </linearGradient>
      <linearGradient
        id="dTool"
        x1={9.437}
        y1={12}
        x2={9.437}
        y2={6.886}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#F06966" />
        <stop offset={1} stopColor="#FAD6A6" />
      </linearGradient>
    </defs>
  </svg>
);

export const LessionIcon = () => (
  <svg width={16} height={16} viewBox="0 0 11 12" fill="none">
    <path
      d="M1.358.016a1.582 1.582 0 0 0-1.3 1.095L0 1.295v4.707c0 4.49.003 4.714.047 4.862.132.427.44.798.81.97.372.176.058.166 4.865.166h4.376l.114-.066a.507.507 0 0 0 .242-.551.642.642 0 0 0-.248-.327c-.042-.024-1.073-.034-4.434-.045l-4.38-.013-.129-.08a.546.546 0 0 1-.245-.613c.042-.153.216-.333.356-.372.07-.021 1.431-.03 4.4-.03 4.113 0 4.303-.002 4.403-.05a.51.51 0 0 0 .277-.318c.019-.072.027-1.424.021-4.303-.008-3.987-.01-4.208-.055-4.346A1.274 1.274 0 0 0 9.603.06L9.439.003 5.484.001C3.31-.002 1.453.006 1.358.016Zm.633 4.93v3.956h-.282c-.254 0-.541.04-.675.092-.043.019-.045-.16-.04-3.786L1.002 1.4l.071-.124a.561.561 0 0 1 .348-.259c.045-.013.193-.02.327-.023l.243-.003v3.955Zm3.557-1.594c.058.02.471.271.914.556.702.45.818.535.886.64.198.312.156.692-.103.929-.094.09-1.547 1.02-1.668 1.073-.248.103-.617.005-.805-.214-.168-.195-.174-.253-.166-1.447.008-1.187.003-1.137.203-1.347a.684.684 0 0 1 .739-.19Z"
      fill="url(#aLession)"
    />
    <defs>
      <linearGradient
        id="aLession"
        x1={5.547}
        y1={-0.069}
        x2={5.626}
        y2={12.063}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#FAD6A6" />
        <stop offset={1} stopColor="#F06966" />
      </linearGradient>
    </defs>
  </svg>
);

export const ClubIcon = () => (
  <svg
    width={20}
    height={16}
    viewBox="0 0 16 12"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M12.278 5.313a1.694 1.694 0 1 0 0-3.387 1.694 1.694 0 0 0 0 3.387Z"
      fill="url(#aClub)"
    />
    <path
      d="M8.89 9.05a3.41 3.41 0 1 1 6.818 0c0 .87-3.406 1.02-3.406 1.02S8.89 10.063 8.89 9.05Z"
      fill="url(#bClub)"
    />
    <path
      d="M3.39 5.313a1.694 1.694 0 1 0 0-3.387 1.694 1.694 0 0 0 0 3.387Z"
      fill="url(#cClub)"
    />
    <path
      d="M.002 9.05a3.41 3.41 0 1 1 6.817 0c0 .87-3.406 1.02-3.406 1.02s-3.41-.007-3.41-1.02Z"
      fill="url(#dClub)"
    />
    <path
      d="M7.799 4.99a2.495 2.495 0 1 0 0-4.99 2.495 2.495 0 0 0 0 4.99Z"
      fill="url(#eClub)"
    />
    <path
      d="M2.809 10.497a5.022 5.022 0 1 1 10.043 0c0 1.28-5.021 1.503-5.021 1.503s-5.022-.011-5.022-1.503Z"
      fill="url(#fClub)"
    />
    <defs>
      <linearGradient
        id="aClub"
        x1={12.278}
        y1={1.926}
        x2={12.278}
        y2={5.313}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#FAD6A6" />
        <stop offset={1} stopColor="#F06966" />
      </linearGradient>
      <linearGradient
        id="bClub"
        x1={8.889}
        y1={7.796}
        x2={15.71}
        y2={7.796}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#FAD6A6" />
        <stop offset={1} stopColor="#F06966" />
      </linearGradient>
      <linearGradient
        id="cClub"
        x1={3.389}
        y1={1.926}
        x2={3.389}
        y2={5.313}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#FAD6A6" />
        <stop offset={1} stopColor="#F06966" />
      </linearGradient>
      <linearGradient
        id="dClub"
        x1={0}
        y1={7.796}
        x2={6.822}
        y2={7.796}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#FAD6A6" />
        <stop offset={1} stopColor="#F06966" />
      </linearGradient>
      <linearGradient
        id="eClub"
        x1={7.799}
        y1={0}
        x2={7.799}
        y2={4.99}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#FAD6A6" />
        <stop offset={1} stopColor="#F06966" />
      </linearGradient>
      <linearGradient
        id="fClub"
        x1={2.808}
        y1={8.71}
        x2={12.852}
        y2={8.71}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#FAD6A6" />
        <stop offset={1} stopColor="#F06966" />
      </linearGradient>
    </defs>
  </svg>
);

export const BackButtonIcon = () => (
  <svg width={40} height={40} viewBox="0 0 40 40" fill="none">
    <circle cx={20} cy={20} r={20} fill="#3B3B3D" />
    <circle cx={20} cy={20} r={19.5} stroke="#000" strokeOpacity={0.15} />
    <path
      d="M15.819 16.8c-1.82 1.874-2.745 2.87-2.787 3.001a.57.57 0 0 0 0 .376c.097.263 5.398 5.708 5.641 5.795.493.163.98-.426.743-.902-.037-.075-1.005-1.102-2.143-2.287l-2.08-2.142h5.792c4.139 0 5.836-.019 5.946-.07.365-.175.432-.763.122-1.064l-.159-.15-5.835-.032-5.836-.031 2.1-2.162c2.281-2.349 2.293-2.368 2.068-2.787-.121-.239-.316-.345-.627-.345-.213 0-.34.125-2.945 2.8Z"
      fill="url(#aBack)"
    />
    <defs>
      <linearGradient
        id="aBack"
        x1={13}
        y1={14}
        x2={25.5}
        y2={26}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#16A9B1" />
        <stop offset={1} stopColor="#B5D654" />
      </linearGradient>
    </defs>
  </svg>
);
