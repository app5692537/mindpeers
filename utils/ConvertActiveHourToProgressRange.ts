type convertActiveHourToProgressRangeProps = {
  startingRange: number;
  endingRange: number;
  startTime: number;
  endTime: number;
  startActiveHr: number;
  endActiveHr: number;
};

/**
 *
 * ConvertActiveHourToProgressRange takes all parameters and give start and end points to display progress bar
 * @returns returns the { startingPoint, endingPoint }
 */

export const ConvertActiveHourToProgressRange = ({
  startingRange,
  endingRange,
  startTime,
  endTime,
  startActiveHr,
  endActiveHr,
}: convertActiveHourToProgressRangeProps) => {
  let startingPoint = 0;
  let endingPoint = 0;

  // Ensure the active hours are within the provided range
  const clampedStartActiveHr = Math.max(startActiveHr, startTime);
  const clampedEndActiveHr = Math.min(endActiveHr, endTime);

  if (
    startActiveHr < startTime ||
    endActiveHr > endTime ||
    endActiveHr < startActiveHr
  ) {
    return { startingPoint, endingPoint };
  }

  // Calculate the progress within the provided range
  const totalRange = endingRange - startingRange;
  const progressStart =
    ((clampedStartActiveHr - startTime) / (endTime - startTime)) * totalRange;
  const progressEnd =
    ((clampedEndActiveHr - startTime) / (endTime - startTime)) * totalRange;

  // Calculate starting and ending points
  startingPoint = Math.min(
    Math.max(progressStart + startingRange, startingRange),
    endingRange
  );
  endingPoint = Math.min(
    Math.max(progressEnd + startingRange, startingRange),
    endingRange
  );

  return { startingPoint, endingPoint };
};
