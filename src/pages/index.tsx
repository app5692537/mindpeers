import FlowPeaks from "@/components/FlowPeaks";
import ToolKit from "@/components/ToolKit";
import Header from "@/components/core/Header";
import React, { memo } from "react";

const Home = () => {
  return (
    <div className="w-full max-w-7xl mx-auto">
      <Header headerTitle="Flow Zone" />
      <div className="flex flex-col lg:flex-row justify-between">
        <FlowPeaks />
        <ToolKit />
      </div>
    </div>
  );
};

export default memo(Home);
