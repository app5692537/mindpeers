import React, { memo } from "react";
import Mark from "./core/Mark";
import { FLOW_PEAK, TIMES_OF_DAY } from "../../utils/constants";
import { ChevronIcon, InfoIcon } from "../../utils/icons";
import FlowPeakCard from "./core/FlowPeakCard";
import CircularProgress from "./core/CircularProgress";
import { Range } from "../../utils/Range";

const FlowPeaks = () => {
  const renderMarks = () => {
    const marks = [];
    for (let num = 0; num <= 48; num++) {
      marks.push(
        <Mark
          key={num}
          angle={num * 7.5}
          type={num % 4 === 0 ? "hour" : "minute"}
          digit={num / 2}
        />
      );
    }
    return marks;
  };

  const ranges = Range();

  return (
    <div className="flex flex-col justify-center items-center lg:w-1/2">
      <div className="w-full px-6 mb-6">
        <div className="flex justify-between items-center bg-[#2C2C2E] rounded-xl shadow-lg border-[0.5px] border-opacity-40 border-[#8C8C8C] px-4 py-3">
          <span className="text-white text-xl font-bold">About Flow Zone</span>
          <ChevronIcon />
        </div>
      </div>

      <div className="flex justify-between items-start mb-8 w-full px-6">
        <div className="flex items-center">
          <h4 className="text-white text-xl font-bold mr-2">Flow Peaks</h4>
          <InfoIcon />
        </div>
        <div className="text-white px-3 py-2 text-base bg-[#393939] rounded-md">
          Weekly
        </div>
      </div>
      <div className="relative py-14 px-[85px] md:py-20 md:px-32 flex justify-center items-center">
        <div className="absolute flex justify-center items-center z-10 -rotate-[45deg]">
          <CircularProgress circularWidth={220} ranges={ranges} />
        </div>
        {TIMES_OF_DAY.map((timeOfDay, index) => {
          const getPositionStyle = () => {
            switch (timeOfDay.position) {
              case "top-0":
                return { top: 0 };
              case "right-0":
                return { right: 0 };
              case "bottom-0":
                return { bottom: 0 };
              case "left-0":
                return { left: 0 };
              default:
                return {};
            }
          };
          return (
            <div
              key={index}
              className={`absolute text-[#E1E1E1] text-sm md:text-xl`}
              style={getPositionStyle()}
            >
              {timeOfDay.name}
            </div>
          );
        })}
        <div className="absolute h-[300px] outline-[#E1E1E1] outline-1 outline-dashed rotate-45 z-10 opacity-60" />
        <div className="absolute h-[300px] outline-[#E1E1E1] outline-1 outline-dashed -rotate-45 z-10 opacity-60" />
        <div className="circle">
          <div className="circle-face">{renderMarks()}</div>
        </div>
      </div>

      <div className="mt-8 flex justify-center items-center">
        {FLOW_PEAK.map((flow) => {
          return (
            <FlowPeakCard
              key={flow.duration}
              duration={flow.duration}
              title={flow.title}
            />
          );
        })}
      </div>
    </div>
  );
};

export default memo(FlowPeaks);
