import React from "react";
import { GloballyIcon, LocationIcon } from "../../utils/icons";
import { TOOLKIT_DATA } from "../../utils/constants";

function ToolKit() {
  return (
    <div className="p-4 lg:w-1/2 mt-10 lg:mt-0">
      <h4 className="text-white text-xl font-bold lg:text-center">
        My Thriving Toolkit
      </h4>
      <div className="flex justify-center mt-10">
        <table className="table-border">
          <thead>
            <tr>
              <th></th>
              <th className="text-white text-base text-center pb-2">
                <div className="flex justify-center items-center">
                  <LocationIcon />
                  <p className="ml-2">For You</p>
                </div>
              </th>
              <th className="text-white text-base text-center pb-2">
                <div className="flex justify-center items-center">
                  <GloballyIcon />
                  <p className="ml-2">Globally</p>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            {TOOLKIT_DATA.map((toolkit) => (
              <tr key={toolkit.name}>
                <td className="text-white text-base p-2.5 text-center">
                  <div className="flex justify-start items-center">
                    {toolkit.icon()}
                    <p className="ml-2">{toolkit.name}</p>
                  </div>
                </td>
                <td className="text-white text-base font-bold p-2.5 text-center">
                  {toolkit.forYou}
                </td>
                <td className="text-white text-base font-bold p-2.5 text-center">
                  {toolkit.globally}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default ToolKit;
