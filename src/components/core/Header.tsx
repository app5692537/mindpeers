import React from "react";
import { BackButtonIcon } from "../../../utils/icons";

type HeaderTypes = {
  headerTitle: string;
  showBackBtn?: boolean;
};

const Header = ({ headerTitle, showBackBtn = true }: HeaderTypes) => {
  return (
    <header className="flex items-center justify-between md:justify-start my-6 px-6">
      {showBackBtn && <BackButtonIcon />}
      <h4 className="text-white text-2xl font-bold ml-4 text-center block md:flex">
        {headerTitle}
      </h4>
      <div className="md:hidden" />
    </header>
  );
};

export default Header;
