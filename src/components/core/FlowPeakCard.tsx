import React from "react";
import { ClockIcons } from "../../../utils/icons";

export type FlowPeakCardTypes = {
  title: string;
  duration: string;
};

function FlowPeakCard({ duration, title }: FlowPeakCardTypes) {
  return (
    <div className="bg-[#2C2C2E] rounded-xl border-[0.5px] border-opacity-40 border-[#8C8C8C] px-4 py-3 mx-2 md:mx-4">
      <p className="text-white text-base">{title}</p>
      <div className="flex items-center mt-1">
        <ClockIcons />
        <p className="text-white text-base ml-2">{duration}</p>
      </div>
    </div>
  );
}

export default FlowPeakCard;
