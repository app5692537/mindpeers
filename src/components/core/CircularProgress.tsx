import React, { memo, useEffect, useState } from "react";

export interface IRange {
  startingPoint: number;
  endingPoint: number;
}

function CircularProgress({
  circularWidth,
  ranges,
}: {
  circularWidth: number;
  ranges: IRange[];
}) {
  const radius = 85;
  const dashArray = radius * Math.PI * 2;

  const [animatedBars, setAnimatedBars] = useState<number[]>([]);

  useEffect(() => {
    // Animate each progress bar individually
    ranges.forEach((range, index) => {
      const animatedBar = setTimeout(() => {
        setAnimatedBars((prev) => [...prev, index]);
      }, 500);
      return () => clearTimeout(animatedBar);
    });
  }, [ranges]);

  return (
    <div className="">
      <svg
        className="w-[270px] h-[270px] md:w-[300px] md:h-[300px]"
        viewBox={`0 0 ${circularWidth} ${circularWidth}`}
      >
        <defs>
          <linearGradient id="progress" x1="0" y1="1" x2="1" y2="0">
            <stop offset="0%" stopColor="#FFCAC9"></stop>
            <stop offset="100%" stopColor="#737DFE"></stop>
          </linearGradient>
        </defs>

        <circle
          cx={circularWidth / 2}
          cy={circularWidth / 2}
          strokeWidth="20px"
          r={radius}
          stroke="#454545"
          fill="none"
        />

        {ranges.map((range, index) => {
          const isAnimated = animatedBars.includes(index);
          const adjustedStartingPoint = (dashArray * range.startingPoint) / 100;
          const newAdjustedStartingPoint = adjustedStartingPoint + 9;
          const adjustedEndPoint = (dashArray * range.endingPoint) / 100;
          const adjustedDashArray =
            adjustedEndPoint - adjustedStartingPoint - 18;

          const initialOffset = isAnimated
            ? -newAdjustedStartingPoint
            : -dashArray;

          return (
            <circle
              key={index}
              cx={circularWidth / 2}
              cy={circularWidth / 2}
              strokeWidth="20px"
              r={radius}
              stroke="url(#progress)"
              style={{
                fill: "none",
                strokeLinecap: "round",
                strokeLinejoin: "round",
                strokeDasharray: `${adjustedDashArray} ${dashArray}`,
                strokeDashoffset: initialOffset,
                transition: "stroke-dashoffset 2s ease-in-out",
              }}
            />
          );
        })}
      </svg>
    </div>
  );
}

export default memo(CircularProgress);
