import React from "react";

type IMark = {
  angle: number;
  type: "hour" | "minute";
  digit: number;
};

const Mark = ({ angle, type, digit }: IMark) => {
  return (
    <div
      className={`circle-face-mark circle-face-mark--${type} relative z-20`}
      style={{ transform: `rotate(${angle}deg)` }}
    >
      <div
        className={`flex flex-col ${
          type === "hour"
            ? "justify-between h-[40px]"
            : "justify-center h-[20px]"
        } items-center`}
      >
        <div
          className={`w-[2px] rounded-md ${
            type === "hour" ? "h-[15px]" : "h-[10px]"
          } ${type === "hour" ? "bg-white" : "bg-[#939395]"}`}
        />
        {type === "hour" && (
          <p
            className={`${digit === 24 ? "hidden" : "flex"} ${
              digit === 0 || digit === 6 || digit === 12 || digit === 18
                ? "text-white text-sm"
                : "text-[#939395] text-xs"
            }`}
            style={{ transform: `rotate(${(48 - digit * 2) * 7.5}deg)` }}
          >
            {digit}
          </p>
        )}
      </div>
    </div>
  );
};

export default Mark;
